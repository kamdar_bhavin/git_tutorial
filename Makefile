CC = gcc
CFLAGS	= -g -Wall

CSRCS	= $(wildcard *.c)
OBJS = $(CSRCS:.c=.o)

.PHONY: clean

all: test

test: $(OBJS)
		  $(CC) $(CFLAGS) -o test $(OBJS)

%.o: %.c
			$(CC) -c $(CFLAGS) $< -o $@

clean:
	    rm -rf *.o
	    rm -rf test
